﻿using Microsoft.EntityFrameworkCore;
using AbcLinguasApi.Models;

namespace AbcLinguasApi.Dados
{
    public class AbcLinguasContext : DbContext
    {
        public AbcLinguasContext(DbContextOptions<AbcLinguasContext> options) : base(options)
        {
        }      


        public DbSet<Aluno> Alunos { get; set; }
        public DbSet<Professor> Professores { get; set; }

    }
}
