import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './App.css';

export class Professor extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      Professor: [],
      modalAberta: false
    };

    this.buscarProfessor = this.buscarProfessor.bind(this);
    this.buscarprofessor = this.buscarprofessor.bind(this);
    this.atualizarprofessor = this.atualizarprofessor.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarProfessor();
  }

  // GET (todos Professor)
  buscarProfessor() {
    fetch('https://localhost:5001/api/Professores')
      .then(response => response.json())
      .then(data => this.setState({ Professor: data }));
  }
  
  //GET (professor com determinado id)
  buscarprofessor(id) {
    fetch('https://localhost:5001/api/Professores/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirprofessor = (professor) => {
    fetch('https://localhost:5001/api/Professores', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(professor)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarProfessor();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarprofessor(professor) {
    fetch('https://localhost:5001/api/Professores/' + professor.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(professor)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarprofessor();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirprofessor = (id) => {
    fetch('https://localhost:5001/api/Professores/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarProfessor();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarprofessor(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const professor = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirprofessor(professor);
    } else {
      this.atualizarprofessor(professor);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do professor</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do professor' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type='email' placeholder='Email' value={this.state.email} onChange={this.atualizaEmail} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Nome do professor</th>
            <th>E-mail</th>
          </tr>
        </thead>
        <tbody>
          {this.state.Professor.map((professor) => (
            <tr key={professor.id}>
              <td>{professor.nome}</td>
              <td>{professor.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(professor.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirprofessor(professor.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar professor</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
};